module Plugin.Util where

import           Data.Function           ((&))
import           Data.Text               as T

import           Foreign.C               (withCString)

import           Godot.Api               (Godot_ClassDB (..))
import           Godot.Gdnative.Internal
import           Godot.Gdnative.Types
import           Godot.Methods           (get_class, instance')


godotPrint :: String -> IO ()
godotPrint str = godot_print `submerge` (T.pack str)

mkClassInstance :: Text -> IO GodotObject
mkClassInstance className = do
  classDB <- Godot_ClassDB <$> getSingleton "ClassDB"
  cls <- instance' classDB `submerge` className
         >>= fromGodotVariant
  get_class cls
    >>= fromLowLevel
    >>= godotPrint . mappend "I made a " . T.unpack
  return cls

getSingleton :: Text -> IO GodotObject
getSingleton name = godot_global_get_singleton
                    & withCString (T.unpack name)


-- Type conversions

dip
  :: (GodotFFI low high, GodotFFI low' high')
  => (low -> IO low')
  -> high
  -> IO high'
dip lowf high = lowf `submerge` high >>= fromLowLevel

submerge :: GodotFFI low high => (low -> IO a) -> high -> IO a
submerge lowf high = toLowLevel high >>= lowf

withVariant :: AsVariant a => (a -> IO b) -> GodotVariant -> IO b
withVariant f v = fromGodotVariant v >>= f
