{-# LANGUAGE TypeFamilies #-}
module Plugin.CubeMaker where

import           Data.Function           ((&))
import           Data.Vector             ((!?))

import           Godot.Api
import           Godot.Gdnative.Internal
import           Godot.Gdnative.Types
import           Godot.Internal.Dispatch
import qualified Godot.Methods           as Godot
import           Godot.Nativescript

import           Plugin.Types
import           Plugin.Util



--- TYPES

data CubeMaker = CubeMaker
  { _cmObj      :: GodotObject
  , _cmChildren :: [GodotNode]
  }

instance GodotClass CubeMaker where
  godotClassName = "CubeMaker"

instance ClassExport CubeMaker where
  classInit obj = return $ CubeMaker obj []
  classExtends = "Node"
  classMethods = [ Func NoRPC "_ready" ready
                 , Func NoRPC "_physics_process" physicsProcess
                 , Func NoRPC "make_cube" makeCube
                 ]

instance HasBaseClass CubeMaker where
  type BaseClass CubeMaker = GodotNode
  super (CubeMaker obj _) = GodotNode obj



--- FUNCTIONS


ready :: GodotFunc CubeMaker
ready _ self _ = do
  godotPrint "_ready from Haskell"
  _ <- (\m -> Godot.call self m []) `dip` "make_cube"
  toLowLevel VariantNil

physicsProcess :: GodotFunc CubeMaker
physicsProcess _ self args = do
  delta <- args !? 1 & maybe (return 0) fromGodotVariant :: IO Float
  godotPrint ("Delta is " ++ show delta)
  (GodotNode msh) <- Godot.get_child self 0
  Godot.rotate_x (GodotMeshInstance msh) delta
  toLowLevel VariantNil

makeCube :: GodotFunc CubeMaker
makeCube _ self _ = do
  msh  <- mkClassInstance "CubeMesh"
  mshi <- mkClassInstance "MeshInstance"
  Godot.set_mesh (GodotMeshInstance mshi) (GodotMesh msh)
  Godot.add_child self mshi True
  (toLowLevel . VariantString) `submerge` "Wee"
